//
let fs = require('fs');
let path = require('path');

// ueditor server config
let UEditorConfig;
try{
  UEditorConfig = fs.readFileSync(path.join(__dirname, './config.json'));
  UEditorConfig = UEditorConfig.toString().replace(/\/\*[\s\S]+?\*\//g, "");
  UEditorConfig = JSON.parse(UEditorConfig);
} catch(e) {
  UEditorConfig = {};
}
global.UEditorConfig = UEditorConfig;// nodejs全局变量
