let express = require('express');
let router = express.Router();

// 上传触发器（全局变量后）
let uploadAction = require('./actions/action_upload');

/* GET */
router.get('/', (req, res, next) => {
  let actionType = req.query.action;
  switch (actionType){
    /* 获取配置信息 */
    case 'config':
      res.json(UEditorConfig);
      break;
    /* 列出图片 */
    case 'listimage':
    /* 列出文件 */
    case 'listfile':
      require('./actions/action_list')(req, res, next);
      break;
    default:
      res.status(404).json({
        state: 'ERROR',
        msg: 'Action 没有找到'
      });
      break;
  }
});

/* POST 上传操作 */
router.post('/', (req, res, next) => {
  let actionType = req.query.action, upConfig, fieldName;
  // 上传配置
  switch (actionType){
    /* 上传图片 */
    case 'uploadimage':
      upConfig = {
        path: UEditorConfig['imagePathURL'],
        maxSize: UEditorConfig['imageMaxSize'],
        allowFiles: UEditorConfig['imageAllowFiles']
      };
      fieldName = UEditorConfig['imageFieldName'];
      break;
    /* 上传涂鸦 */
    case 'uploadscrawl':
      upConfig = {
        path: UEditorConfig['scrawlPathURL'],
        maxSize: UEditorConfig['scrawlMaxSize'],
        allowFiles: UEditorConfig['scrawlAllowFiles']
      };
      fieldName = UEditorConfig['scrawlFieldName'];
      // base64
      break;
    /* 上传视频 */
    case 'uploadvideo':
      upConfig = {
        path: UEditorConfig['videoPathURL'],
        maxSize: UEditorConfig['videoMaxSize'],
        allowFiles: UEditorConfig['videoAllowFiles']
      };
      fieldName = UEditorConfig['videoFieldName'];
      break;
    /* 上传文件 */
    case 'uploadfile':
      upConfig = {
        path: UEditorConfig['filePathURL'],
        maxSize: UEditorConfig['fileMaxSize'],
        allowFiles: UEditorConfig['fileAllowFiles']
      };
      fieldName = UEditorConfig['fileFieldName'];
      break;
  }
  uploadAction(upConfig, fieldName)(req, res, next);
});

module.exports = router;
