//
class Base {
  constructor(DBModule) {
    this.db = DBModule;
  }
  // 生效数据库
  save() {
    this.db.write();
  }
  // 增加项目
  add(option) {
    let db_tmp = this.db.push(option);
    db_tmp.write();
    return option;
  }
  // 删除已有
  delete(option) {
    let db_tmp = this.db.remove(option);
    db_tmp.write();
    return option;
  }
  // 查询列表
  findAll(option) {
    let db_tmp = this.db.filter(option);
    // .sortBy('views').take(5)
    return db_tmp.value();
  }
  // 单个详情
  find(option) {
    return this.db.find(option).value();
  }
  // 修改信息
  update(oldVal, newVal) {
    let db_tmp = this.db.find(oldVal).assign(newVal);
    db_tmp.write();
    return this.db.find(oldVal).value();
  }
}

module.exports = Base;
