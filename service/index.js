const path = require('path');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');// 同步

// 数据库
const db_adapter = new FileSync(path.join(__dirname, '../DB/db.json'));
const db = low(db_adapter);

// Set some defaults (required if your JSON file is empty)
db.defaults({
  users: [
    {u: 'giadmin', p: 'giadmin'}
  ],
  project_list: [],
  lastBackupTime: 0
}).write();

// DB property
// db.get db.set db.unset

let ProjectClass = require('./project.js');
let projectModel = new ProjectClass(db.get('project_list'));

let UserClass = require('./user.js');
let userModel = new UserClass(db.get('users'));

module.exports = {
  db,
  userModel,
  projectModel
}
