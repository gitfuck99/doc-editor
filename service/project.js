
const path = require('path');
const fse = require('fs-extra');
let Base = require('./base.js');
let Tools = require('../tools');

let OBJ_Module = require('./module/projectsModule.js');

class Project extends Base {
  constructor(db) {
    super(db);
  }
  addProject(option) {
    delete option.uuid;
    option = Object.assign({}, OBJ_Module(), option);
    let pathDIR = Tools.getProjectSourcePath(option.uuid);
    try {
      fse.ensureFileSync(pathDIR);
    } catch(e) {
      return false;
    }
    return this.add(option);
  }
  findProjectInfo(option) {
    return this.find(option);
  }
  editProjectInfo(option) {
    option.updateTime = Date.now();
    return this.update({uuid: option}, option);
  }
  deleteProject(option) {
    console.log(option);
    return this.delete(option);
  }
}

module.exports = Project;
