
let Base = require('./base.js');

class User extends Base {
  constructor(db) {
    super(db);
  }
  findUser(option) {
    return this.find(option);
  }
}

module.exports = User;
