
let shortid = require('js-shortid');

//
function gen() {
  let ID = shortid.gen();
  return {
    uuid: ID,
    name: '',
    title: '',
    status: 1,
    publish: false,
    publishURL: '',
    createTime: Date.now(),
    updateTime: Date.now()
  };
}

module.exports = gen;
