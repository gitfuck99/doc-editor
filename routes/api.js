var express = require('express');
var router = express.Router();
let fse = require('fs-extra');
let multer = require('multer');
let os = require("os");

let Tools = require('../tools');

let formDataMulter = multer({ dest: os.tmpdir() });

//
let { publishByContent, publishByHtml, toSaveStyle, deleteProject } = require('./funs-module/apiFuns.js');

// 啥也没有
router.get('/', (req, res, next) => {
  res.json({
    host: req.get("host")
  });
});

/**
 * 删除项目
 */
router.delete('/prj/:uuid', (req, res, next) => {
  let uuid = req.params.uuid, statusCode = 200, msg = "SUCCESS";
  try{
    deleteProject({ uuid });
  } catch(e){
    statusCode = 500;
    msg = 'ERROR';
    console.log(e);
  }
  res.status(statusCode).json({
    state: msg
  });
});
/**
 * 检查是否存在样式tag文件
 */
router.get('/checkstylefile', async (req, res, next) => {
  let uuid = req.query.pid || '', statusCode = 200, retCode = 200, msg = "SUCCESS",
  pathDIR = Tools.getSourceRoot(uuid, global.upStyleTagsFileName);
  try{
    if(!await fse.pathExists(pathDIR)){
      retCode = 404;
      msg = 'NOT FOUND';
    }
  } catch(e) {
    statusCode = 500;
    msg = 'ERROR';
    console.log(e);
  }
  // xhr返回
  res.status(statusCode).json({
    code: retCode,
    state: msg,
    uuid
  });
});
/**
 * 获取样式并存到resource中
 */
router.post('/project-stylefile', formDataMulter.any(),(req, res, next) => {
  let body = req.body, statusCode = 200, msg = "SUCCESS";
  try{
    let file = req.files[0];
    toSaveStyle({
      uuid: body.pid,
      fileTmpPath: file.path,
      toSavePth: Tools.getSourceRoot(body.pid, global.upStyleTagsFileName)
    });
  } catch(e) {
    statusCode = 500;
    msg = 'ERROR';
    console.log(e);
  }
  // xhr返回
  res.status(statusCode).json({
    state: msg,
    uuid: body.pid
  });
});
/**
 * 实时保存编辑的源
 */
router.post('/project-source', (req, res, next) => {
  let reqBody = req.body, statusCode = 200, msg = "SUCCESS",
    pathDIR = Tools.getProjectSourcePath(reqBody.pid);
  try{
    fse.outputFileSync(pathDIR, reqBody.content);
  } catch(e) {
    statusCode = 500;
    msg = 'ERROR';
    console.log(e);
  }
  res.status(statusCode).json(msg);
});
/**
 * 文章发布
 */
router.get('/project-publish', (req, res, next) => {
  let uuid = req.query.pid, statusCode = 200, msg = "SUCCESS",
  pathDIR = Tools.getProjectSourcePath(uuid);
  try{
    publishByContent({
      req: req,
      uuid,
      pathDIR,
      styleTagsPath: Tools.getSourceRoot(uuid, global.upStyleTagsFileName)
    });
  } catch(e) {
    statusCode = 500;
    msg = 'ERROR';
    console.log(e);
  }
  // xhr返回
  res.status(statusCode).json({
    state: msg,
    uuid,
    url: `${req.protocol}://${req.get('host')}/view/${uuid}`
  });
});
/**
 * 发布完整的html文件
 */
router.post('/project-publish', formDataMulter.any(), (req, res, next) => {
  let body = req.body, statusCode = 200, msg = "SUCCESS";
  try{
    let file = req.files[0];
    console.log(body.pid);
    console.log(file.path);
    publishByHtml({
      uuid: body.pid,
      pathDIR: file.path
    });
  } catch(e){
    statusCode = 500;
    msg = 'ERROR';
    console.log(e);
  }
  res.status(statusCode).json({
    state: msg,
    uuid: body.pid,
    url: `${req.protocol}://${req.get('host')}/view/${body.pid}`
  });
});

module.exports = router;