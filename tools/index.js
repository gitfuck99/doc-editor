//
const path = require('path');
let UEditorConfig = global.UEditorConfig;

module.exports = {
  getSourceRoot() {
    return path.resolve('.', UEditorConfig.upBaseURL, ...arguments);
  },
  getSourceUpPath() {
    return path.resolve('.', UEditorConfig.upBaseURL, UEditorConfig.upSaveURL);
  },
  getProjectSourcePath(pid) {
    return path.resolve('.', UEditorConfig.upBaseURL, pid, UEditorConfig.upSourceName);
  },
  getPublishPath(pid) {
    return path.resolve('.', UEditorConfig.publishURL, pid);
  },
  // 获取二维码URL
  getQrcodeImageUrl(data, options) {
    options = Object.assign({
      size: '220x220',
      margin: '10'
    }, options);
    return `https://api.qrserver.com/v1/create-qr-code/?data=${encodeURIComponent(data)}&size=${options.size}&margin=${options.margin}`;
  }
}
